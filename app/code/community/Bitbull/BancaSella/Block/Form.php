<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_BancaSella_Block_Form extends Mage_Payment_Block_Form
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bitbull/bancasella/gestpay/form.phtml');
    }

    /**
     * Metodo che verifica se la richiesta è di tipo ajax
     * @return bool
     */
    public function isAjaxRequest(){
        return $this->getRequest()->isXmlHttpRequest();
    }


}