<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_BancaSella_Block_Redirect  extends Mage_Page_Block_Redirect
{

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     *  Get target URL
     *
     *  @return string
     */
    public function getTargetURL ()
    {
        if(!$this->getCalculateTargetUrl()){
            $helper = Mage::helper('bitbull_bancasella');
            $this->setCalculateTargetUrl( $helper->getRedirectUrlToPayment($this->getOrder()));
        }
        return $this->getCalculateTargetUrl();

    }


    public function getMethod ()
    {
        return 'GET';
    }

    public function getMessage ()
    {
        return $this->__('A breve sarai reindirizzato su BancaSella, attendi un attimo');
    }

}
