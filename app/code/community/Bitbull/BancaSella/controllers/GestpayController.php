<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */
class Bitbull_BancaSella_GestpayController extends Mage_Core_Controller_Front_Action {

    private $_order;

    public function getOrder()
    {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }


    public function redirectAction(){
        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        $order->addStatusHistoryComment(  $this->__('Reindirizzamento su Gestpay avvenuto'));
        $order->save();

        $_helper= Mage::helper('bitbull_bancasella');
        $_helper->log('Reindirizzamento utente sul sito di bancasella dopo aver effettuato l\'ordine con id='.$order->getId().' e increment id='.$order->getIncrementId());

        Mage::register('current_order', $order);

        try{

            $this->loadLayout();
            $this->renderLayout();

        }catch (Exception $e){
            $_helper->log($e->getMessage());
            $session = Mage::getSingleton('checkout/session');
            $session->addError($this->__('Pagamento non completato a causa di un errore nel pagamento su Banca Sella.'));
            $this->_redirect('checkout/cart');
            return;
        }

    }

    public function resultAction(){

        $a = $this->getRequest()->getParam('a',false);
        $b = $this->getRequest()->getParam('b',false);

        $_helper= Mage::helper('bitbull_bancasella');

        if(!$a || !$b){
            $_helper->log('Accesso alla pagina per il risultato del pagamento non consentito, mancano i parametri di input');
            $this->norouteAction();
            return;
        }

        Mage::register('bancasella_param_a', $a);
        Mage::register('bancasella_param_b', $b);

        $helper= Mage::helper('bitbull_bancasella/crypt');

        if( $helper->isPaymentOk( $a , $b )){
            $_helper->log('L\'utente ha completato correttamente l\'inserimento dei dati su bancasella');

            $redirect = '*/*/success';
        }
        else{
            $_helper->log('L\'utente ha annullato il pagamento');

            $session = Mage::getSingleton('checkout/session');
            $session->addError($this->__('Pagamento non completato su Banca Sella.'));
            $redirect  = 'checkout/cart';
        }

        //se è impostato lo store allora reindirizzo l'utente allo store corretto
        $store= Mage::registry('bitbull_bancasellapro_store_maked_order');
        if($store && $store->getId()){
            $this->redirectInCorrectStore( $store, $redirect );
        }else{
            $this->_redirect($redirect);
        }
    }

    public function s2sAction(){
        $a = $this->getRequest()->getParam('a',false);
        $b = $this->getRequest()->getParam('b',false);

        $_helper= Mage::helper('bitbull_bancasella');

        if(!$a || !$b){
            $_helper->log('Richiesta S2S, mancano i parametri di input');
            $this->norouteAction();
            return;
        }

        Mage::register('bancasella_param_a', $a);
        Mage::register('bancasella_param_b', $b);

        $helper= Mage::helper('bitbull_bancasella/crypt');

        $webservice = $helper->getInitWebservice();

        $webservice->setDecryptParam($a , $b);
        $helper->decriptPaymentRequest ($webservice);

        $orderId = $webservice->getShopTransactionID();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        if($order->getId()){
            $_helper->log('Imposto lo stato dell\'ordine in base al decrypt');

            $helper->setStatusOrderByS2SRequest($order,$webservice);
        }else{
            $_helper->log('La richiesta effettuata non ha un corrispettivo ordine. Id ordine= '.$webservice->getShopTransactionID());

        }

        //restiutisco una pagina vuota per notifica a GestPay
        $this->getResponse()
            ->setBody('<html></html>');
        return;
    }


    public function successAction(){
        $order = $this->getOrder();
        if (!$order->getId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $session = Mage::getSingleton('checkout/session');
        $session->clear();

        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('bitbull_bancasella_gestpay_success_action', array('order_ids' => array($order->getId())));
        $this->renderLayout();
    }

    protected function redirectInCorrectStore($store, $path, $arguments = array())
    {
        $params = array_merge(
            $arguments,
            array(
                '_use_rewrite' => false,
                '_store' => $store,
                '_store_to_url' => true,
                '_secure' => $store->isCurrentlySecure()
            ) );
        $url = Mage::getUrl($path,$params);

        $this->getResponse()->setRedirect($url);
        return;
    }

}