<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_BancaSella_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * metodo per effettuare il log del modulo
     * @param $msg
     */
    public function log($msg) {
        Mage::log($msg, null, 'Bitbull_BancaSella.log', Mage::getStoreConfig('payment/gestpay/log'));
    }

    /**
     * Metodo che restituisce l'url sul quale effettuare il pagamento
     * @param Mage_Sales_Model_Order $order
     *
     * @return bool|string
     */
    function getRedirectUrlToPayment(Mage_Sales_Model_Order $order){

        //recupero la stringa criptata in base ai dati dell'ordine
        $crypt = Mage::helper('bitbull_bancasella/crypt');
        $stringEncrypt = $crypt->getEncryptStringByOrder($order);

        $method = $order->getPayment()->getMethodInstance();

        if($method instanceof Bitbull_BancaSella_Model_Gestpay){
            $url = $method->getRedirectPagePaymentUrl();
            return $this->createUrl($url , array('a'=>$method->getMerchantId(), 'b'=> $stringEncrypt));
        }
        return false;
    }

    function createUrl($url, $param){
        $paramether= '';
        if(count($param)){
            $paramether= '?';
            foreach($param as $name => $value)
            {
                $paramether.=$name.'='.$value.'&';
            }
        }
        $this->log('Url per il pagamento: '.$url.$paramether);
        return $url.$paramether;
    }

    /**
     * Metodo che verifica se la transazione è stata elaborata dal una richiesta server to server
     * @param $order
     *
     * @return bool
     */
    public function isElaborateS2S($order){
        $state = $order->getState();
        if ($state == Mage_Sales_Model_Order::STATE_NEW)
            return false;
        return true;

    }
}