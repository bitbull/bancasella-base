<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */
abstract class Bitbull_BancaSella_Model_Webservice_Abstract extends Mage_Core_Model_Abstract{

    protected $url_home;

    /**
     * metodo che imposta il base url dell'webservice
     * @param $url
     */
    public function setBaseUrl($url){
        $this->url_home = $url;
    }


    abstract public function getWSUrl();
}