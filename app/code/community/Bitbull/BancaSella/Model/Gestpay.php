<?php
/**
 * @category Bitbull
 * @package  Bitbull_BancaSella
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_BancaSella_Model_Gestpay extends Mage_Payment_Model_Method_Abstract {

    //url per i pagamenti reali
    const REAL_PAYMENT_URL = 'https://ecomm.sella.it';
    const REAL_PAYMENT_URL_WSDL = 'https://ecomms2s.sella.it';

    const TEST_PAYMENT_URL = 'https://testecomm.sella.it';

    const PAGE_REDIRECT_TO_PAYMENT ='/pagam/pagam.aspx';


    protected $_code= "gestpay";
    protected $_formBlockType = 'bitbull_bancasella/form';

    protected $_order;

    /**
     * Availability options
     */
    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;
    protected $_isInitializeNeeded      = true;

    public function getCheckout()
    {
        if (empty($this->_checkout)) {
            $this->_checkout = Mage::getSingleton('checkout/session');
        }
        return $this->_checkout;
    }

    /**
     * Restituisce l'url quando il pagamento va a buon fine
     * @return string
     */
    protected function getSuccessURL()
    {
        return Mage::getUrl('bitbull_bancasella/gestpay/success',array('_secure' => Mage::app()->getStore()->isCurrentlySecure()));
    }

    /**
     * Restituisce l'url dove effettuare il redirect sul sito di bancasella
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('bitbull_bancasella/gestpay/redirect', array('_secure' => true));
    }

    /**
     * Metodo che restituisce il merchant id impostato nello store
     * @return mixed
     */
    public function getMerchantId(){
        return  $this->getConfigData ( 'merchant_id' );
    }

    public function getOrderStatusKoUser(){
        return  $this->getConfigData ( 'order_status_ko_user' );
    }
    public function getOrderStatusOkGestPay(){
        return  $this->getConfigData ( 'order_status_ok_gestpay' );
    }
    public function getOrderStatusKoGestPay(){
        return  $this->getConfigData ( 'order_status_ko_gestpay' );
    }

    /**
     * Metodo che restituisce la moneta impostata nello store
     * @return mixed
     */
    public function getCurrency(){
        return  $this->getConfigData ( 'currency' );
    }

    /**
     * Metodo che restituisce la lingua impostata nello store
     * @return mixed
     */
    public function getLanguage(){
        return  $this->getConfigData ( 'language' );
    }

    /**
     * Metodo che restitiusce url per il pagaemento su gestpay
     * @return string
     */
    public function getGestpayUrl()
    {
        $domain = $this->getBaseUrlSella();
        return $domain."/gestpay/pagam.asp";
    }

    /**
     * REstituisce l'url dove effettuare il redirect per il pagamento sul sito di bancasella
     * @return string
     */
    public function getRedirectPagePaymentUrl()
    {
        $domain = $this->getBaseUrlSella();
        return $domain.self::PAGE_REDIRECT_TO_PAYMENT;
    }

    /**
     * metodo che restituisce l'url dove effettuare la verifica 3dsecure
     * @return string
     */
    public function getAuthPage(){
        $url = $this->getBaseUrlSella();
        return $url. '/pagam/pagam3d.aspx';
    }

    /**
     * restituisce l'url di bancasella a seconda se la modalità di sviluppo è attiva oppure no
     * @return string
     */
    public function getBaseUrlSella(){
        $url = self::REAL_PAYMENT_URL;
        if($this->getConfigData('debug'))
            $url = self::TEST_PAYMENT_URL;
        return $url;
    }


    /**
     * restituisce l'url del wsdl di bancasella a seconda se la modalità di sviluppo è attiva oppure no
     * @return string
     */
    public function getBaseWSDLUrlSella(){
        $url = self::REAL_PAYMENT_URL_WSDL;
        if($this->getConfigData('debug'))
            $url = self::TEST_PAYMENT_URL;
        return $url;
    }

    /**
     * Metodo che restituisce il totale dell'ordine da passare al a bancasella
     * @param $order
     * @return float
     */
    public function getTotalByOrder($order){
        //recupero la currency di default
        $defaultCurrency = $this->getConfigData(
            'currency',
            Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId()
        );
        //e quella dello store attuale
        $storeCurrency = $this->getConfigData('currency');

        /** @var $order Mage_Sales_Model_Order */
        //se sono diverse
        if($defaultCurrency != $storeCurrency){
            //si deve utilizzare la valuta dello store
            return $order->getGrandTotal();
        }else{
            //se sono uguali o hanno la stessa configurazione oppure è stato scelto solo di usare il pagamento di default

            //quindi restituisco il base grand total
            return $order->getBaseGrandTotal();
        }

    }

}